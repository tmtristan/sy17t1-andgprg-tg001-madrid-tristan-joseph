﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float MinX, MaxX, MinY, MaxY;
}
public class ShipController : MonoBehaviour {

    public Boundary Boundary;
    public GameObject Shot;
    public GameObject Missile;
    public Transform ShotSpawn;
    public Transform MissileSpawn;
    public Transform DoubleSpawn;
    public float FireRate;
    public float MissileFireRate;
    public int UpgradeCount;
    public float SpeedFactor; //for speed up upgrade to make sense
    public bool MissileEnabled; //for missile upgrade
    public bool DoubleEnabled; //for double upgrade

    private Vector3 screenPoint;
    private Vector3 offset;
    private bool mouseDown;
    private float nextFire;
    private float nextMissile;
    private HUDUIController uiScript;

    // Use this for initialization
    void Start () {
        mouseDown = false;
        GameObject HUDController = GameObject.FindWithTag("HUDController");
        if (HUDController != null)
        {
            uiScript = HUDController.GetComponent<HUDUIController>();
        }
        if (HUDController == null)
        {
            Debug.Log("Cannot find 'HUDUIController' script");
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (SystemInfo.deviceType == DeviceType.Handheld)
        {
            // Touch here
            if (Input.touchCount > 0)
            {
                //Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
                //transform.position = new Vector3(worldPosition.x, worldPosition.y, transform.position.z);

                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    // Do something
                }
            }
        }
        else if (SystemInfo.deviceType == DeviceType.Desktop)
        {
            if (mouseDown)
                OnMouseDrag();
        }

        ShotSpawner();
        //check if missile is upgraded
        if (MissileEnabled)
            MissileSpawner();

    }

    void ShotSpawner()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + FireRate;
            Instantiate(Shot, ShotSpawn.position, ShotSpawn.rotation);

            //check if double is upgraded
            if (DoubleEnabled)
                Instantiate(Shot, DoubleSpawn.position, DoubleSpawn.rotation);
        }
    }

    void MissileSpawner()
    {
        if (Time.time > nextMissile)
        {
            nextMissile = Time.time + MissileFireRate;
            Instantiate(Missile, MissileSpawn.position, MissileSpawn.rotation);
        }
    }

    //for windows testing
    void OnMouseDown()
    {
        mouseDown = true;
        //Debug.Log("Mouse held");
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }

    void OnMouseDrag()
    {
        Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
        //limit player to boundary
        cursorPosition.x = Mathf.Clamp(cursorPosition.x, Boundary.MinX, Boundary.MaxX);
        cursorPosition.y = Mathf.Clamp(cursorPosition.y, Boundary.MinY, Boundary.MaxY);

        //transform.position = cursorPosition; if code below breaks, use this
        //this will limit ship speed due to Speed Factor
        transform.position = Vector3.Lerp(transform.position, cursorPosition, SpeedFactor);
    }

    void OnMouseUp()
    {
        mouseDown = false;
        //Debug.Log("Mouse released");
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.tag == "Powerup")
        {
            if (UpgradeCount == 3)
                UpgradeCount = 0;

            UpgradeCount++;
            
        }
    }

    void OnDestroy()
    {
        uiScript.Alive = false;
    }
}
