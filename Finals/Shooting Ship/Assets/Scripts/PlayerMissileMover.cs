﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMissileMover : MonoBehaviour {

    public float Speed;
    public Vector3 MaxRotationVector;
    public float RotateWait;

    private Rigidbody2D rb;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.right * Speed;
        StartCoroutine(Rotate());
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    IEnumerator Rotate()
    {
        yield return new WaitForSeconds(RotateWait);
        if (transform.eulerAngles.z < MaxRotationVector.z)
            transform.eulerAngles = Vector3.Lerp(transform.rotation.eulerAngles, MaxRotationVector, Time.deltaTime);
        else
            transform.eulerAngles = MaxRotationVector;
    }
}
