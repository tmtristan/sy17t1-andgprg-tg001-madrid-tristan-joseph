﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGScroller : MonoBehaviour {

    public float ScrollSpeed;
    public float TileSizeX;

    private Vector3 startPos;

	// Use this for initialization
	void Start () {
        startPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        float newPos = Mathf.Repeat(Time.time * ScrollSpeed, TileSizeX);
        transform.position = startPos + Vector3.right * newPos;
	}
}
