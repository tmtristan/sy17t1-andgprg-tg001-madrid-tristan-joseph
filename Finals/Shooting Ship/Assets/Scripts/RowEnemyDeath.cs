﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RowEnemyDeath : MonoBehaviour {

    public GameObject UpgradePickup;   

    private RowEnemyKillCounter killCounter;
    private bool isQuitting = false;

    void Awake()
    {
        //this also prevents an error showing up in the log as well, but I need to wait
        //for the coroutine at the GameController to finish since the enemy cannot be destroyed
        //when reloading scenes
        DontDestroyOnLoad(this.gameObject);
    }

    void OnApplicationQuit()
    {
        //this prevents an error showing up in the log which is:
        //"Some objects were not cleaned up when closing the scene. (Did you spawn new GameObjects from OnDestroy?)"
        isQuitting = true;
    }
    private void OnDestroy()
    {        
        killCounter.KillCount++;
        if (killCounter.KillCount == 5 && !isQuitting)
        {
            Instantiate(UpgradePickup, transform.position, transform.rotation);
        }
    }

    public void OnDisable()
    {
        //once killcount reaches 5, spawn upgrade pickup
        
    }

    // Use this for initialization
    void Start () {
        killCounter = GameObject.Find("Row Enemy Kill Counter").GetComponent<RowEnemyKillCounter>();
	}
	// Update is called once per frame
	void Update () {
        
    }
}
