﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HUDUIController : MonoBehaviour {

    public Image SpeedBar;
    public Image MissileBar;
    public Image DoubleBar;
    public ShipController ShipScript;
    public float MaxSpeedFactor;
    public Text ScoreText;
    public bool Alive;
    public GameObject GameOverScreen;
    public Text HighScoreText;
    public GameController GameControllerScript;


    private Animator anim;
    private bool canSpeed;
    private bool canMissile;
    private bool canDouble;
    private int currentScore;
    private int highScore;

    // Use this for initialization
    void Start () {
        currentScore = 0;
        highScore = PlayerPrefs.GetInt("highScore", highScore);
        UpdateScore();
        Alive = true;
        canSpeed = true;
        canMissile = true;
        canDouble = true;
        GameOverScreen.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {

        BarColorChanger();
        UpgradeChecker();

        if (Alive == false)
        {
            GameControllerScript.GameOver = true;
            ShowGameOver();
            UpdateScore();
        }
                
	}

    public void UpgradeButtonClick()
    {
        if (ShipScript.UpgradeCount == 1)
        {
            //if canSpeed is true, upgrade is available
            if (canSpeed)
                ShipScript.SpeedFactor += 0.02f;
            else //else upgrade count will be kept and not wasted on unavailable upgrades
                return;

        }
        else if (ShipScript.UpgradeCount == 2)
        {
            if (canMissile)
                ShipScript.MissileEnabled = true;
            else
                return;
        }
        else if (ShipScript.UpgradeCount == 3)
        {
            if (canDouble)
                ShipScript.DoubleEnabled = true;
            else
                return;
        }
            
        ShipScript.UpgradeCount = 0;
        
    }

    void BarColorChanger()
    {
        if (ShipScript.UpgradeCount == 1)
        {
            DoubleBar.color = new Color(255, 255, 255, 255);
            SpeedBar.color = new Color(0, 234, 255, 255);
        }
        else if (ShipScript.UpgradeCount == 2)
        {
            SpeedBar.color = new Color(255, 255, 255, 255);
            MissileBar.color = new Color(0, 234, 255, 255);
        }
        else if (ShipScript.UpgradeCount == 3)
        {
            MissileBar.color = new Color(255, 255, 255, 255);
            DoubleBar.color = new Color(0, 234, 255, 255);
        }
        else
        {
            SpeedBar.color = new Color(255, 255, 255, 255);
            MissileBar.color = new Color(255, 255, 255, 255);
            DoubleBar.color = new Color(255, 255, 255, 255);
        }
    }

    void UpgradeChecker()
    {
        //checks if upgrades are available to click and sets the disabled animation for each bars

        //check if current speed factor > max speed factor + 0.02;
        if (ShipScript.SpeedFactor > MaxSpeedFactor)
        {
            anim = SpeedBar.GetComponent<Animator>();
            canSpeed = false;
            anim.SetBool("isEnabled", false);
        }
        //check if bool of ship script missile enabled is true
        if (ShipScript.MissileEnabled)
        {
            anim = MissileBar.GetComponent<Animator>();
            canMissile = false;
            anim.SetBool("isEnabled", false);
        }
        //same as above, but with double
        if (ShipScript.DoubleEnabled)
        {
            anim = DoubleBar.GetComponent<Animator>();
            canDouble = false;
            anim.SetBool("isEnabled", false);
        }
    }

    public void AddScore (int newScoreValue)
    {
        currentScore += newScoreValue;
        UpdateScore();
    }

    void UpdateScore()
    {
        ScoreText.text = "SCORE: " + currentScore.ToString();

        if (currentScore >= highScore)
        {
            highScore = currentScore;
            PlayerPrefs.SetInt("highScore", highScore);
        }

        HighScoreText.text = "HIGH SCORE: " + highScore.ToString();
    }

    void ShowGameOver()
    {
        GameOverScreen.gameObject.SetActive(true);
    }

    public void RestartButtonClick()
    {
        SceneManager.LoadScene("stage1");
    }
}
