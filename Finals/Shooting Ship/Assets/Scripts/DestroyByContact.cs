﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {

    public GameObject Explosion;
    public GameObject PlayerExplosion;
    public int RowEnemyScoreValue;
    public int PowerupScoreValue;

    private HUDUIController uiScript;

    void Start()
    {
        GameObject HUDController = GameObject.FindWithTag("HUDController");
        if (HUDController != null)
        {
            uiScript = HUDController.GetComponent<HUDUIController>();
        }
        if (HUDController == null)
        {
            Debug.Log("Cannot find 'HUDUIController' script");
        }
    }

    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.tag == "Boundary")
            return;

        if (tag == "Enemy")
        {
            Instantiate(Explosion, transform.position, transform.rotation);
            if (other.tag == "Player")
                Instantiate(PlayerExplosion, other.transform.position, other.transform.rotation);

            if (other.tag == "PlayerShot")
                uiScript.AddScore(RowEnemyScoreValue);

            Destroy(other.gameObject);
            Destroy(gameObject);
        }  
        
        if (tag == "Powerup")
        {
            if (other.tag == "Player")
            {
                uiScript.AddScore(PowerupScoreValue);
                Destroy(gameObject);
            }
                
        }          
    }
}
