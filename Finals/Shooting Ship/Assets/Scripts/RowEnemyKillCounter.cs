﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RowEnemyKillCounter : MonoBehaviour {

    public int KillCount;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (KillCount == 5)
            KillCount = 0;            
	}
}
