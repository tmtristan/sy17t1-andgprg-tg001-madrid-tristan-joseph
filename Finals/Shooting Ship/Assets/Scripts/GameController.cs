﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public GameObject RowEnemy;
    public float BoundaryWidth;
    public Vector3 SpawnValues;
    public int RowEnemyCount;
    public float SpawnWait;
    public float StartWait;
    public float WaveWait;
    public GameObject RestartButton;
    public bool GameOver;


    private bool restart;

	void Start () {
        GameOver = false;
        restart = false;
        RestartButton.gameObject.SetActive(false);
        RandomizeY();
        StartCoroutine (SpawnWaves());
	}
	
	void Update () {
        //once there are no enemies left, restart button will appear
        //this also prevents enemies staying on screen after reloading a scene
		if (restart)
            RestartButton.gameObject.SetActive(true);
    }

    void RandomizeY()
    {
        SpawnValues.y = Random.Range(-BoundaryWidth, BoundaryWidth);
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(StartWait);
        while(true)
        {
            for (int i = 0; i < RowEnemyCount; i++)
            {
                Vector3 spawnPostion = new Vector3(SpawnValues.x, SpawnValues.y, SpawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(RowEnemy, spawnPostion, spawnRotation);
                yield return new WaitForSeconds(SpawnWait);
            }
            RandomizeY();
            yield return new WaitForSeconds(WaveWait);

            //if GameOver is true, then it will stop spawning new enemies
            if (GameOver)
            {
                restart = true;
                break;
            }
        }
        
        
    }
}
