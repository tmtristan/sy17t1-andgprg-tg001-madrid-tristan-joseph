﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject Player;

    private Vector3 movementVector;
    private Vector3 offset;

    void Start()
    {
        offset = transform.position - Player.transform.position;
    }
    void LateUpdate()
    {
        movementVector.x = Player.transform.position.x + offset.x;
        movementVector.z = Player.transform.position.z + offset.z;

        transform.position = movementVector;
    }
}
