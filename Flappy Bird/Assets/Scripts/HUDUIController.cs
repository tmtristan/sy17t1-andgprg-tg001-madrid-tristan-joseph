﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HUDUIController : MonoBehaviour {

    public Rock Player;
    public Text CurrentScore;
    public Text GameScore;
    public Text HighScore;
    public GameObject GameOverScreen;

    private int current = 0;
    private int high = 0;
    // Use this for initialization
    void Start () {
        GameOverScreen.gameObject.SetActive(false);
        high = PlayerPrefs.GetInt("high", high);
    }
	
	// Update is called once per frame
	void Update () {
        CurrentScore.text = Player.ScoreCount.ToString();
        if (!Player.Alive)
        {
            GameOverScreen.gameObject.SetActive(true);
            GameScore.text = CurrentScore.text;

            current = int.Parse(CurrentScore.text);
            if (high < current)
                high = current;
            HighScore.text = high.ToString();
            PlayerPrefs.SetInt("high", high);
        }
    }

    public void RestartButtonClick()
    {
        SceneManager.LoadScene("flappytenshi");
    }
}
