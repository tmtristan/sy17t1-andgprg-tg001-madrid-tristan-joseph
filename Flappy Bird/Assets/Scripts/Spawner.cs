﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
    public List<GameObject> Spawnables;
    public GameObject Camera;
    public Rock Rock;

    private List<GameObject> spawnedObjects = new List<GameObject>();
    private GameObject spawnedUnit;
    private float xBackPos = -1.5f;
    private float xGroundPos = -0.5f;
    private float xPipePos = 0;
    private float randNum;
    private bool isPlaying;
    private bool hasStartedSpawning;

    // Use this for initialization
    void Start () {
        //spawnedPipe = Instantiate(PipePrefab, new Vector3(10, 0, 0), Quaternion.identity);
        randNum = 0;
        
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
            isPlaying = true;
        if (isPlaying)
        {
            if(!hasStartedSpawning)
            {
                InvokeRepeating("SpawnObject", 0.0f, 1.0f);
                InvokeRepeating("KillObject", 1.0f, 1.0f);
                hasStartedSpawning = true;
            }            
            randNum = Random.Range(-1.5f, 1.5f);

            if (!Rock.Alive)
                CancelInvoke();
        }
        
    }

    void SpawnObject ()
    {
        spawnedUnit = Instantiate(Spawnables[0], new Vector3(xBackPos, 0.34f, 0), Quaternion.identity);//background
        spawnedObjects.Add(spawnedUnit);
        spawnedUnit = Instantiate(Spawnables[1], new Vector3(xPipePos, 6 + randNum, 0), Quaternion.identity);
        spawnedObjects.Add(spawnedUnit);
        spawnedUnit = Instantiate(Spawnables[2], new Vector3(xPipePos, -4 + randNum, 0), Quaternion.identity);
        spawnedObjects.Add(spawnedUnit);
        spawnedUnit = Instantiate(Spawnables[4], new Vector3(xPipePos, 1.03f + randNum, 0), Quaternion.identity);//score trigger
        spawnedObjects.Add(spawnedUnit);
        spawnedUnit = Instantiate(Spawnables[3], new Vector3(xGroundPos, -4.15f, 0.0f), Quaternion.identity);
        spawnedObjects.Add(spawnedUnit);
        xBackPos += 5;
        xPipePos += 5;
        xGroundPos += 6.23f;


    }
    void KillObject()
    {
        List<GameObject> destroyedObjects = new List<GameObject>();
        foreach(GameObject spawnedUnit in spawnedObjects)
        {
            if (spawnedUnit.transform.position.x < Camera.transform.position.x - 5.0f)
            {
                destroyedObjects.Add(spawnedUnit);
                Destroy(spawnedUnit);
            }                
        }

        foreach(GameObject destroyedObject in destroyedObjects)
        {
            spawnedObjects.Remove(destroyedObject);
        } 
    }
}
