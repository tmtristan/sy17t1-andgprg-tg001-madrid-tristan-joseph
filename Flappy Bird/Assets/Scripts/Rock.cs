﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour {

    public float JumpForce;
    public float XSpeed;
    public int ScoreCount;
    public bool Alive;

    private Rigidbody2D rb;
    private Vector2 movementVector;    
    private bool isPlaying;
    private Animator anim;

	// Use this for initialization
	void Start () {
        // Caching
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        isPlaying = false;
        Alive = true;
        rb.gravityScale = 0;
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.Space))
        {
            isPlaying = true;
            rb.gravityScale = 1;
        }
            

        if (Alive)
        {            
            if (Input.GetKeyDown(KeyCode.Space))
            {
                anim.SetBool("isAlive", true);
                //rb.AddForce(new Vector2(0, JumpForce));
                rb.velocity = new Vector2(0, JumpForce);
            }            
        }       

        if (isPlaying)            
            movementVector.x = XSpeed;    
                           
        movementVector *= Time.deltaTime;
        transform.Translate(movementVector);

        //if (Input.GetKey(KeyCode.W))
        //      {
        //          transform.Translate(0, Speed, 0);
        //      }
        //      if (Input.GetKey(KeyCode.S))
        //      {
        //          transform.Translate(0, -Speed, 0);
        //      }
        //      if (Input.GetKey(KeyCode.A))
        //      {
        //          transform.Translate(-Speed, 0, 0);
        //      }
        //      if (Input.GetKey(KeyCode.D))
        //      {
        //          transform.Translate(Speed, 0, 0);
        //      }
    }

    void OnCollisionEnter2D (Collision2D col)
    {
        anim.SetBool("isAlive", false);
        Alive = false;
        XSpeed = 0;
    }

    void OnTriggerEnter2D (Collider2D col)
    {
        ScoreCount++;
        //Debug.Log("Score: " + ScoreCount);
    }
}
